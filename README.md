<h1>Plugin "Artículos Plus"</h1>

<strong>Ubicación del plugin:</strong> facturascripts/plugins/articulos_plus

<br>

<strong>Descripción</strong>

Plugin para añadir atributos a un artículo

<br>

<strong>Características:</strong>

<ul>
   <li>Añade URL a una imagen de artículo</li>
   <li>Añade categoría web</li>
</ul>

<br>

<strong>Sugerencias en consideración:</strong>

<ul>
   <li></li>
</ul>

<br>

<strong>Apoya el plugin:</strong>

Puedes apoyar el desarrollo del plugin sugiriendo y enviando cambios.
