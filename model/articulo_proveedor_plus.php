<?php

require_model("proveedor.php");
require_model("articulo.php");
require_model("articulo_plus.php");

class articulo_proveedor_plus extends fs_model
{
   var $referenciaplus;//la compone la referencia _ codproveedor
   var $referencia;
   var $codproveedor;
   var $pvd;
   var $stock;

   /**
    * @var proveedor proveedor
    */
   var $proveedor;

   /**
    * @var articulo articulo
    */
   var $articulo;

   public function __construct($data = FALSE)
   {
      parent::__construct('articulo_proveedor_plus', 'plugins/articulos_plus/');

      if ($data)
      {
         $this->referenciaplus = $data['referenciaplus'];
         $this->referencia = $data['referencia'];
         $this->codproveedor = $data['codproveedor'];
         $this->pvd = floatval($data['pvd']);
         $this->stock = $this->intval($data['stock']);
      }
   }
   
   public function get_iva()
   {
       $articulo=new articulo();
       $articulo = $articulo->get($this->referencia);
       if($articulo)
           return $articulo->get_iva();
        
   }
   
  public function pvd_iva($pvd,$iva)
  {
      if($iva!==FALSE)
        if($iva===0)
            return $pvd;
        elseif($iva>0)
            return round($pvd+($pvd*$iva/100), 3);
      else
        return false;
  }
   
   
  public function get_pvd_iva()
  {
      if($this->get_iva()!==FALSE)
        if($this->get_iva()===0)
            return $this->pvd;
        elseif($this->get_iva()>0)
            return round($this->pvd+($this->pvd*$this->get_iva()/100), 3);
      else
        return false;
  }

   /**
    * Esta función es llamada al crear una tabla.
    * Permite insertar valores en la tabla.
    */
   protected function install()
   {
      return '';
   }

   /**
    * Esta función devuelve TRUE si los datos del objeto se encuentran
    * en la base de datos.
    */
   public function exists()
   {
      if ($this->referenciaplus)
      {
         $value = $this->var2str($this->referenciaplus);
         return $this->db->select("SELECT * FROM {$this->table_name} WHERE referenciaplus = $value");
      }

      return false;
   }

   /**
    * Esta función sirve tanto para insertar como para actualizar
    * los datos del objeto en la base de datos.
    */
   public function save()
   {
      $sql = "";
      if ($this->exists())
      {
         $value = $this->var2str($this->referenciaplus);
         if ($this->referenciaplus)
         {
            $sql = "UPDATE {$this->table_name} SET referencia = " . $this->var2str($this->referencia) . "," .
                    "codproveedor = " . $this->var2str($this->codproveedor) . ", " .
                    "pvd = " . $this->var2str($this->pvd) . ", " .
                    "stock = " . $this->var2str($this->stock) . "
                          WHERE referenciaplus = $value";
            return $this->db->exec($sql);
         }
      }
      else
      {
         $sql = "INSERT INTO {$this->table_name} ( referenciaplus,referencia,codproveedor, pvd, stock ) VALUES (" .
                 $this->var2str($this->referenciaplus) . ", " .
                 $this->var2str($this->referencia) . ", " .
                 $this->var2str($this->codproveedor) . ", " .
                 $this->var2str($this->pvd) . ", " .
                 $this->var2str($this->stock) . ")";
         return $this->db->exec($sql);
      }

      return false;
   }

   /**
    * Esta función sirve para eliminar los datos del objeto de la base de datos
    */
   public function delete()
   {
      $value = $this->var2str($this->referenciaplus);
      if ($this->referenciaplus)
      {
         $sql = "DELETE FROM {$this->table_name} WHERE referenciaplus = $value)";
         return $this->db->exec($sql);
      }
   }

   /**
    * Devuelve todos los datos de los objetos en la base de datos
    */
   public function get($cod)
   {
      $art = $this->db->select("SELECT * FROM " . $this->table_name . " WHERE referenciaplus = " . $this->var2str($cod) . ";");
      if ($art) {
         return new articulo_proveedor_plus($art[0]);
      } else {
         return FALSE;
      }
   }
   
   public function getmejor($ref)
   {
      $art = $this->db->select("SELECT * FROM " . $this->table_name . " WHERE referencia = " . $this->var2str($ref) . " AND stock>0 ORDER BY pvd;");
      if ($art) {
         return new articulo_proveedor_plus($art[0]);
      } else {
          $art = $this->db->select("SELECT * FROM " . $this->table_name . " WHERE referencia = " . $this->var2str($ref) . " ORDER BY pvd;");
          if ($art) {
          return new articulo_proveedor_plus($art[0]);
          } else {
             return FALSE;
          }
      }
   }
   
   public function getmejorarray($ref)
   {
      $art = $this->db->select("SELECT pvd,stock,codproveedor FROM " . $this->table_name . " WHERE referencia = " . $this->var2str($ref) . " AND stock>0 ORDER BY pvd;");
      if ($art) {
         return $art[0];
      } else {
          $art = $this->db->select("SELECT pvd,stock,codproveedor FROM " . $this->table_name . " WHERE referencia = " . $this->var2str($ref) . " ORDER BY pvd;");
          if ($art) {
          return $art[0];
          } else {
             return FALSE;
          }
      }
   }
   
   public function getmejorpvd($ref)
   {
      $art = $this->db->select("SELECT pvd FROM " . $this->table_name . " WHERE referencia = " . $this->var2str($ref) . " AND stock>0 ORDER BY pvd;");
      if ($art) {
         return $art[0]['pvd'];
      } else {
          $art = $this->db->select("SELECT pvd FROM " . $this->table_name . " WHERE referencia = " . $this->var2str($ref) . " ORDER BY pvd;");
          if ($art) {
          return $art[0]['pvd'];
          } else {
             return FALSE;
          }
      }
   }
   
   public function getmejorstock($ref)
   {
      $art = $this->db->select("SELECT stock FROM " . $this->table_name . " WHERE referencia = " . $this->var2str($ref) . " AND stock>0 ORDER BY pvd;");
      if ($art) {
         return $art[0]['stock'];
      } 
      else
      {
        return FALSE;
      }
   }
   
   
   
   public function getitems($ref)
   {
      return $this->parse($this->db->select("SELECT * FROM {$this->table_name} WHERE referencia = {$this->var2str($ref)}"));
   }
   
   

   /**
    * Devuelve todos los objetos de la base de datos con paginacion
    */
   public function get_all_offset($offset = 0, $limit = FS_ITEM_LIMIT)
   {
      return $this->parse($this->db->select_limit("SELECT * FROM {$this->table_name} ORDER BY referenciaplus DESC", $limit, $offset), true);
   }

   /**
    * Devuelve todos los objetos en la base de datos sin paginacion
    */
   public function get_all()
   {
      return $this->parse($this->db->select("SELECT * FROM {$this->table_name} ORDER BY referenciaplus DESC"), true);
   }
   
   public function nomProveedor()
   {
       $proveedor= new proveedor();
       $proveedor=$proveedor->get($this->codproveedor);
       return $proveedor->nombre;
   }
   

   
   /**
    * Convierte los objetos devueltos en un array de elementos del tipo de la clase
    */
   public function parse($items, $array = false)
   {
         $list = array();
         foreach ($items as $item)
         {
            $list[] = new articulo_proveedor_plus($item);
         }
         if($list[0])
            return $list;
      return null;
   }

}
