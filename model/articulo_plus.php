<?php

/*
 * This file is part of FacturaSctipts
 * Copyright (C) 2013-2015  Carlos Garcia Gomez  neorazorx@gmail.com
 * Forked Francisco Javier Trujillo Jimenez
 * Copyright (C) 2015  Francesc Pineda Segarra  shawe.ewahs@gmail.com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_model('albaran_cliente.php');
require_model('albaran_proveedor.php');
require_model('articulo_proveedor_plus.php');
require_model('familia.php');
require_model('impuesto.php');
require_model('stock.php');

/**
 * Almacena los datos extra de un artículos.
 */
class articulo_plus extends fs_model
{

   /**
    * Clave primaria. Varchar (18).
    * @var type 
    */
   public $referencia;
   
   

   /**
    * Define el tipo de artículo, así se pueden establecer distinciones
    * según un tipo u otro.
    * @var type Varchar (10).
    */
   public $cat_web;
   public $url_imagen;
   public $descripcion_larga;
   private $exists;
   public $sql;

   public function __construct($a = FALSE)
   {
      parent::__construct('articulos_plus', 'plugins/articulos_plus/');

      if ($a)
      {
         $this->referencia = $a['referencia'];
         $this->descripcion_larga = $a['descripcion_larga'];
         $this->url_imagen = $a['url_imagen'];
         $this->cat_web = $a['cat_web'];
         $this->exists = TRUE;
      }
      else
      {
         $this->referencia = NULL;
         $this->descripcion_larga = '';
         $this->url_imagen = '';
         $this->cat_web = '';
         $this->exists = FALSE;
      }

      $this->pvp_ant = 0;
      $this->iva = NULL;
   }

   /**
    * FALTA DESCRIBIR QUE HACE
    */
   protected function install()
   {
      /* la tabla articulos tiene claves ajeas a familias, impuestos y stocks
        new familia();
        new impuesto(); */

      return '';
   }

   /**
    * FALTA DESCRIBIR QUE HACE
    */
   public function get_descripcion_64()
   {
      return base64_encode($this->descripcion);
   }

   /**
    * FALTA DESCRIBIR QUE HACE
    */
   public function get($ref)
   {
      $art = $this->db->select("SELECT * FROM " . $this->table_name . " WHERE referencia = " . $this->var2str($ref) . ";");
      if ($art) {
         return new articulo_plus($art[0]);
      } else {
         return FALSE;
      }
   }

   /**
    * Esta función devuelve TRUE si el artículo ya existe en la base de datos.
    * Por motivos de rendimiento y al ser esta una clase de uso intensivo,
    * se utiliza la variable $this->exists para almacenar el resultado.
    * @return type
    */
   public function exists()
   {
      if (!$this->exists)
      {
         if ($this->db->select("SELECT referencia FROM " . $this->table_name . " WHERE referencia = " . $this->var2str($this->referencia) . ";"))
         {
            $this->exists = TRUE;
         }
      }

      return $this->exists;
   }

   /**
    * Esta función sirve tanto para insertar como para actualizar
    * los datos del objeto en la base de datos.
    */
   public function save()
   {
      if ($this->exists())
      {
         $sql = "UPDATE " . $this->table_name . " SET descripcion_larga = " . $this->var2str($this->descripcion_larga) . ", " .
                 "url_imagen = " . $this->var2str($this->url_imagen) . ", " .
                 "cat_web = " . $this->var2str($this->cat_web) .
                 " WHERE referencia = " . $this->var2str($this->referencia) . ";";
      }
      else
      {
         $sql = "INSERT INTO " . $this->table_name . " (referencia, url_imagen, cat_web, descripcion_larga) VALUES (" .
                 $this->var2str($this->referencia) . "," .
                 $this->var2str($this->url_imagen) . "," .
                 $this->var2str($this->cat_web) . "," .
                 $this->var2str($this->descripcion_larga) . ");";
      }

      if ($this->db->exec($sql))
      {
         $this->exists = TRUE;
         return TRUE;
      }
      else
      {
         return FALSE;
      }
   }
   
   /**
    * Esta función sirve para eliminar los datos del objeto de la base de datos
    */
   public function delete()
   {
      $sql = "DELETE FROM " . $this->table_name . " WHERE referencia = " . $this->var2str($this->referencia) . ";";
      if ($this->db->exec($sql))
      {
         $this->exists = FALSE;
         return TRUE;
      }
      else
      {
         return FALSE;
      }
   }

   public function imagen_url()
   {
      if( $this->has_imagen )
      {
         if( file_exists('tmp/articulos_plus/'.$this->referencia.'.png') )
         {
            return 'tmp/articulos_plus/'.$this->referencia.'.png';
         }
         else
         {
            if( is_null($this->imagen) )
            {
               $imagen = $this->db->select("SELECT url_imagen FROM ".$this->table_name." WHERE referencia = ".$this->var2str($this->referencia).";");
               if($imagen)
               {
                  $this->imagen = $this->str2bin($imagen[0]['url_imagen']);
               }
               else
                  $this->has_imagen = FALSE;
            }
            
            if( isset($this->imagen) )
            {
               if( !file_exists('tmp/articulos') )
                  mkdir('tmp/articulos');
               
               $f = fopen('tmp/articulos_plus/'.$this->referencia.'.png', 'a');
               fwrite($f, $this->imagen);
               fclose($f);
               return 'tmp/articulos_plus/'.$this->referencia.'.png';
            }
            else
               return FALSE;
         }
      }
      else
         return FALSE;
   }
   
   public function set_imagen($img)
   {
      if( is_null($img) )
      {
         $this->imagen = NULL;
         $this->has_imagen = FALSE;
         $this->clean_image_cache();
      }
      else
      {
         $this->imagen = $img;
         $this->has_imagen = TRUE;
      }
   }
   
   
}
