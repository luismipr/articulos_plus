<?php

/*
 * This file is part of FacturaScripts
 * Copyright (C) 2015  Carlos Garcia Gomez  neorazorx@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_model('articulo.php');
require_model('articulo.php');
require_model('proveedor.php');
require_model('articulo_plus.php');
require_model('articulo_proveedor_plus.php');
//esto lo he comentado, pero creo que no influye
//require_model('articulo_propiedad.php'); 

/**
 * Description of documentos_facturas
 *
 * @author carlos
 */
class articulos_proveedor_plus extends fs_controller
{
   public $articulo;
   public $articulo_plus;
   public $articulo_proveedor_plus;
   public $mejor_proveedor;
   public function __construct()
   {
      parent::__construct(__CLASS__, 'Articulos Proveedor Plus', 'ventas', FALSE, FALSE);
   }

   protected function private_core()
   {
      $this->share_extension();
      
      $this->articulo = FALSE;
      if( isset($_REQUEST['ref']) )
      {
         $art0 = new articulo();
         $this->articulo = $art0->get($_REQUEST['ref']);
      }
      
      if( isset($_GET['ref']) )
      {
         $articulo_proveedor_plus = new articulo_proveedor_plus();
         $this->articulo_proveedor_plus = $articulo_proveedor_plus->getitems($_GET['ref']);
         $this->mejor_proveedor= $articulo_proveedor_plus->getmejor($_GET['ref']);
      }
      else
         $this->new_error_msg('Artículo no encontrado');
   }
   
   

   /**
    * Añade las extensiones "articulos_plus_(cli/prov) a (ventas/compras)_articulos"
    */
   private function share_extension()
   {
      $fsext = new fs_extension();
      $fsext->name = 'articulos_plus_proveedor_cli';
      $fsext->from = __CLASS__;
      $fsext->to = 'ventas_articulo';
      $fsext->type = 'tab';
      $fsext->text = '<span class="glyphicon glyphicon-comment" aria-hidden="true" title="Articulos Proveedor Plus"></span><span class="hidden-xs"> &nbsp; Articulos Proveedor Plus</span>';
      $fsext->save();
   }

}
