<?php

/*
 * This file is part of FacturaScripts
 * Copyright (C) 2015  Carlos Garcia Gomez  neorazorx@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_model('articulo.php');
require_model('articulo_plus.php');
//esto lo he comentado, pero creo que no influye
//require_model('articulo_propiedad.php'); 

/**
 * Description of documentos_facturas
 *
 * @author carlos
 */
class ficha extends fs_controller
{
   public $articulo;
   public $articulo_plus;

   public function __construct()
   {
      parent::__construct(__CLASS__, 'Ficha', 'ventas', FALSE, FALSE);
   }

   protected function private_core()
   {
      
      $this->articulo = FALSE;
      if( isset($_REQUEST['ref']) )
      {
         $art0 = new articulo();
         $this->articulo = $art0->get($_REQUEST['ref']);
      }
      
      if( isset($_GET['ref']) )
      {
         $articulo_plus = new articulo_plus();
         $this->articulo_plus = $articulo_plus->get($_GET['ref']);
         if($this->articulo_plus)
         {
            $this->new_message("Articulo encontrado");
            $this->imagenes=  explode(",", $this->articulo_plus->url_imagen);
            $this->articulo_plus->url_imagen=$this->imagenes[0];
         }
         else 
         {
             $this->new_error_msg("No encuentra el articulos");
         }
      }
      else if( isset($_POST['referencia']) )
      {
         $art0 = new articulo();
         $this->articulo = $art0->get($_POST['referencia']);
         //aqui hay que llamar al constructor y no estaba puesto
         $articulo_plus = new articulo_plus();
         $this->articulo_plus = $articulo_plus->get($_POST['referencia']);
         //--------------------------------------------------------------------
         if($this->articulo)
         {
            if(!$this->articulo_plus)//si el get no nos devuelve nada enemos que añadir un nuevo articulo_plus
            {
                $articulo_plus = new articulo_plus();
                //le penogo ya misma referiencia del articulo esto no se recibe ya que es el unico compo que no se puede editar.
                $this->articulo_plus->referencia = $this->articulo->referencia;
            }
            
            if( !isset($_POST['url_imagen']) )
            {
                //correccion aqui lo estabas armacenando en arrays en vez de en objetos se ha corregido todos los campos
               $this->articulo_plus->url_imagen = '';
            }
            else
            {
               $this->articulo_plus->url_imagen = $_POST['url_imagen'];
            }
            
            if( !isset($_POST['cat_web']) )
            {
               $this->articulo_plus->cat_web = '';
            }
            else
            {
               $this->articulo_plus->cat_web = $_POST['cat_web'];
            }
            
            if( !isset($_POST['descripcion_larga']) )
            {
               $this->articulo_plus->descripcion_larga = $this->articulo->descripcion;
            }
            else
            {
               $this->articulo_plus->descripcion_larga = $_POST['descripcion_larga'];
            }
            
         }
      }
      else
         $this->new_error_msg('Artículo no encontrado');
   }

   /**
    * Añade las extensiones "articulos_plus_(cli/prov) a (ventas/compras)_articulos"
    */


}
